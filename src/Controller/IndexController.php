<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{

    private $menu = [
        'Marke' =>
            ['Historie', 'Familienunternehmen'],
        'Produktwelt' =>
            ['Rasierer', 'Rasierpinsel']
    ];

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'menu' => $this->menu,
        ]);
    }
}
